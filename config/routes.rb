Rails.application.routes.draw do

  mount_devise_token_auth_for 'User',at: 'api/v1/auth'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  namespace :api do
    namespace :v1 do
      resources :jobs, only: [:index, :create, :destroy, :update]

      resources :job_applications

      get '/all_job_applications', to: 'job_applications#all_job_applications'
      post '/view_job_application', to: 'job_applications#view_job_application'

    end
  end
end
