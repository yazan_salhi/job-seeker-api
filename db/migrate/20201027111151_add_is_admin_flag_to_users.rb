class AddIsAdminFlagToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :is_admin, :boolean, null: false, default: 0
  end
end
