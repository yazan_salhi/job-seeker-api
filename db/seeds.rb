# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.new(email: "admin@jobs.com", password: "admin@123", is_admin: true)
admin.save!

normal_user = User.new(email: "yazan@jobs.com", password: "yazan@123", is_admin: false)
normal_user.save!

job = Job.new(title:"Back-end",description:"Ruby and rails")
job.save!
