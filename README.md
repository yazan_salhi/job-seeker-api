
### Postman collection
https://www.getpostman.com/collections/8977f88ec4cba5b3a6ab

## Heruko link
https://yazan-job-seeker-api.herokuapp.com

### Clone repo

```shell

git clone https://yazan_salhi@bitbucket.org/yazan_salhi/job-seeker-api.git
cd job-seeker-api
```


### Ruby version

ruby 2.6.3

### Install dependencies
yarn
bundle install
### Initialize the database
create database.yml in config dir
Copy database.yml.example
then
Paste on database.yml

```shell
  cp config/database.yml.example config/database.yml;
```

```shell
rails db:migrate; rails db:seed;
```

## finaly start the project

```shell
rails s
```
