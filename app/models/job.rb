class Job < ApplicationRecord
    # validations
    validates :title, :description, presence: true
end
