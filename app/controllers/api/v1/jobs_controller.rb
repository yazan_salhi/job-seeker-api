class Api::V1::JobsController < ApplicationController
  authorize_resource :job
  before_action :authenticate_user!

  def index
    jobs = Job.all
    render json: jobs,  status: 200
    
  end

  def show
    job = Job.find(params[:id])
    render json: job, status: 200
  end

  def create

    job = Job.new(job_params)
    job.save
    render json: job,status: 201
  end

  def update
    job = Job.find_by!(id: params[:id])
    job.update(job_params)
    render json: job, status: 204
  end

  def destroy
    job = Job.find_by!(id: params[:id])
    job.destroy
    render json: job, status: 202
  end

  def job_params
    params.require(:job).permit(:title, :description)
  end
 
end
