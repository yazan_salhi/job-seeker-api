class Api::V1::JobApplicationsController < ApplicationController
  before_action :authenticate_user!

  def show
    job_applicaiton = JobApplication.find(params[:job_id])
    render json: job, status: 200
  end

  def create
    job_applicaiton = JobApplication.new(job_application_params)
    job_applicaiton.save
    render json: job_applicaiton,status: 201
  end

  def view_job_application
    if current_user.is_admin?
      job_applicaiton = JobApplication.where(:user_id => params[:user_id]).where(:job_id => params[:job_id]).first
      job_applicaiton.status = true
      job_applicaiton.save
      render json: job_applicaiton,status: 200
  end
end

  def all_job_applications
    if current_user.is_admin?
      render json: JobApplication.all ,status: 201
    else
      render json:current_user.job_applications,status: 201
    end
  end

  def job_application_params
    params.require(:job_application).permit(:user_id, :job_id)
  end
end
